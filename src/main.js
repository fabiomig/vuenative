import Vue from 'nativescript-vue';

var firebase = require("nativescript-plugin-firebase");

import router from './router';

import store from './store';

import './styles.scss';

// Uncommment the following to see NativeScript-Vue output logs
Vue.config.silent = false;

var firebase = require("nativescript-plugin-firebase");

// NativeScript needs to start before we can reliably invoke Firebase plugin features
setTimeout(function() {
  firebase.init({
    onAuthStateChanged: function(data) { // optional but useful to immediately re-logon the user when he re-visits your app
      console.log(data.loggedIn ? "Logged in to firebase" : "Logged out from firebase");
      if (data.loggedIn) {
        console.log("user's email address: " + (data.user.email ? data.user.email : "N/A"));
      }
    }
  }).then(
      function (instance) {
        console.log("firebase.init done");
          firebase.login({
            type: firebase.LoginType.GOOGLE
          }).then(
              function (result) {
                console.log("login result: " + JSON.stringify(result));
              },
              function (errorMessage) {
                console.log("login error: " + errorMessage);
              }
          );
      },
      function (error) {
        console.log("firebase.init error: " + error);
      }
  );
});

new Vue({

  router,

  store,

}).$start();
